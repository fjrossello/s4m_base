#!/bin/sh

for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done


ini_file_template () {
  modulename="$1"
  modulevers="$2"
  echo "# Module name (no spaces, alphanumeric + underscore only)
Name=$modulename
# Module command namespace. Convention: Short single word related to module name, all lower case
Namespace=$modulename
# Module version string
Version=$modulevers
# Module author
Author=A.Person
# Module is enabled (1 or 0)
Enabled=1
# Module description (one line, no wrapping)
Description=My Module which does foo and bar
# Optional version history. Format: <version>:<date>:<changes>
# Can wrap over multiple lines
VersionHistory=$modulevers:yyyymmdd:Initial version
# Comma separated list of <command>:<script_file_name>
CommandRoutes=foo:foo.sh,bar:bar.sh
# Comma separated list of <command>:[<opt_label>]:<opt_description>:<OPTIONAL|MANDATORY>:[<opt_flag>],...
# Can wrap over multiple lines
CommandOptions=
# Comma separated binary dependencies in format <command>:<binary_or_command_name_or_path>,...
# Can wrap over multiple lines.
CommandDepends=
# Module dependencies, comma separated. Format:  <module_name>[==|>=<module_version>],...
# The module version and qualifier parts are optional.
# Can wrap over multiple lines.
# Example: pipeline>=0.1,foo>=2.1,bar
# Example: pipeline,foo,bar>=3.3
ModuleDepends=
"
}


user_script_template () {
  modulename="$1"
  echo "#/bin/sh
# Module: $modulename
# Author: A.Person

# Include S4M functions (mandatory unless you're doing a \"Hello world\" module
# and don't need any API functions)
for i in \`ls \$S4M_SCRIPT_HOME/inc/*.sh\`; do . \$i; done

## ============================================================================

### Your code here ###
main () {

  ## Import other module functions etc  
  #s4m_import "othermodule/script.sh"

  ## Route command(s) from module.ini to custom shell functions defined in this
  ## shell script or included by this shell script.
  ##
  ## Route order does not matter - either one will execute or none will.
  ##
  ## This script will auto-exit immediately after a route is executed because
  ## you can only run a single command at a time.
  ##
  ## This is the recommended way to map commands to shell wrapper functions
  ## to do your work.
  ##
  s4m_route "mycommand" "my_function"
  s4m_route "anothercommand" "another_function"

  ## .. OR remove the above lines and implement your own logic flow
  ## based on the command name in variable  \$S4M_MOD_CMD

  ## Options (flags) specified in module.ini are captured and available to you
  ## by accessing variables named like  \$<module_name>_<longflagname>
  ##
  ## You can also access your full shell arguments in variable  \$S4M_MOD_OPTS
  ## to do as you please, if you prefer.
  ##
}

### START ###
main
"
}


Usage () {
  echo
  echo "Usage: $0 <module_path> <module_name> [<module_version>]"; echo
  echo "   NOTE: If not specified, module version will be '0.1'"; echo
  exit 1
}


main () {
  modulepath="$1"
  modulename="$2"
  modulevers="$3"
  if [ -z "$modulevers" ]; then
    modulevers="0.1"
  fi

  if [ -z "$modulepath" -o -z "$modulename" ]; then
    Usage
  fi

  if [ ! -d "$modulepath" ]; then
    s4m_error "Module path '$modulepath' does not exist!"
    exit 1
  fi
  if [ ! -w "$modulepath" ]; then
    s4m_error "Module path '$modulepath' not writeable!"
    exit 1
  fi

  pathchk --portability "$modulename" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    s4m_error "Error: Module name '$modulename' contains non-portable characters!"
    echo "  Please use only alphanumeric and underscore (no spaces)."; echo
    exit 1
  fi

  if [ -d "$modulepath/$modulename" ]; then
    s4m_error "Module '$modulename' already exists!"
    exit 1
  fi

  ## The module directory is created here! 
  mkdir -p "$modulepath/$modulename/$modulevers"

  if [ $? -ne 0 ]; then
    s4m_error "Could not create module path '$modulepath/$modulename/$modulevers!"
    exit 1
  fi

  ini_file_template "$modulename" "$modulevers" > "$modulepath/$modulename/$modulevers/module.ini"
  user_script_template "$modulename" > "$modulepath/$modulename/$modulevers/$modulename.sh"
}


## MAIN ##
main "$1" "$2" "$3"

