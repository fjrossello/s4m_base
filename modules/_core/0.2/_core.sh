#/bin/sh
# Module: _core
# Author: Othmar Korn

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

. ./inc/core_functions.sh


## Route the help command appropriately
## e.g. following user command:
##
##   s4m help foo
##
## If "foo" is a module, return its help documentation
## (module description and all of its command help)
##
## If "foo" is a command, return help documentation for that command only, or
## request for clarification (e.g. bar::foo, foo::foo)
##
## TODO: T#2439: In-progress
##
core_help_handler () {
  
  #s4m_debug "Got command = [$S4M_MOD_CMD]"
  #s4m_debug "Got args for help command = [$S4M_MOD_OPTS]"

  ## No arguments to "help" command - invoke built-in help
  if [ -z "$S4M_MOD_OPTS" ]; then
    s4m_exec --help
    return $?
  fi

  echo "$S4M_MOD_OPTS" | grep " " > /dev/null 2>&1
  ## No spaces in string, it could be a module
  if [ $? -ne 0 ]; then
    echo " ( searching ..) "
    possible_module="$S4M_MOD_OPTS"
    ## suppress any error messages here since we want nice output in case of bad user query
    if s4m_check_module_exists "$possible_module" "$S4M_MODULE_PATH" 2>/dev/null; then
      ## print module information
      core_get_module_help_info "$possible_module" "$S4M_MODULE_PATH" 
      return
    fi
  fi

  ## Not a module or module command, search term/s not found
  s4m_log "Sorry, No help information found for query '$S4M_MOD_OPTS'"

  ## Not found
  return 1
}


core_addmodule_handler () {

  #s4m_debug "addmodule_handler(): Got name=[$_core_name], version=[$_core_version]"

  ## Default module version
  if [ -z "$_core_version" ]; then
    _core_version=0.1
  fi

  ## Create module
  $SHELL "$S4M_THIS_MODULE/scripts/addmodule.sh" "$S4M_MODULE_PATH" "$_core_name" "$_core_version"
  ret=$?

  if [ $ret -eq 0 ]; then
    s4m_log "Created new module '$_core_name' in path '$S4M_MODULE_PATH/$_core_name'"
  else
    s4m_error "Failed to create new module"
  fi

  return $ret
}


main () {

  s4m_route addmodule core_addmodule_handler
  s4m_route help core_help_handler

}

### START ###
main

